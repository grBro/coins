class AvailableCoins
  attr_reader :box

  def initialize(box_remainder_session_value)
    if box_remainder_session_value
      @box = box_remainder_session_value.map { |k, v| [k.to_i, v] }.to_h
    else
      @box = {50 => 10, 25 => 7, 10 => 2, 5 => 100, 2 => 100, 1 => 50}
    end
  end

  def remove_coin(coin_value)
    self.box[coin_value] = box[coin_value] - 1
  end

  def coins_left(coin_value)
    box[coin_value]
  end

  def box_remainder
    box
  end
end
