class CoinsExchange < ActiveRecord::Base
  class CoinException < StandardError; end
  attr_accessor :box_remainder_session_value

  COINS_LIST = [1, 2, 5, 10, 25, 50]

  validates :amount, presence: true
  validate :coin_types_in_coins_list,
           if: proc { |c| c.coin_types.present? }

  def exchange
    coins_count = coins_ary
    coins_used = coins_ary
    calculate_optimal_exchange(coins_count, coins_used)
    get_exchange_result(coins_count, coins_used)
  end

  def calculate_optimal_exchange(coins_count, coins_used)
    0.upto(amount) do |cents|
      coin_counter = cents
      new_coin = 1
      clist.select { |c| c <= cents }.each do |j|
        if 1 + coins_count[cents - j][j] < coin_counter
          coin_counter = 1 + coins_count[cents - j][j]
          new_coin = j
        end

        coins_count[cents][j] = coin_counter
        coins_used[cents][j] = new_coin
      end
    end
  end

  def get_exchange_result(coins_count, coins_used)
    coin = amount
    this_coin = []
    available_coins = AvailableCoins.new(box_remainder_session_value)
    while coin > 0
      begin
        j = coins_count[coin].group_by { |_k, v| v }
                             .reject { |k, _v| k == 0 }
                             .min_by { |k, _v| k }.last.to_h.keys.first
      rescue StandardError
        raise CoinException, 'Not enough coins!'
      end

      if available_coins.coins_left(j) <= 0
        coins_count.each { |c| c[j] = 0 }
        next
      end

      available_coins.remove_coin(j)
      this_coin << coins_used[coin][j]
      coin -= this_coin.last
    end

    {change: this_coin.group_by { |x| x }.map { |k, v| [k, v.count] }.to_h,
     box_remainder: available_coins.box_remainder}
  end

  private

  def coins_ary
    Array.new(amount + 1) { Hash[COINS_LIST.map { |x| [x, 0] }] }
  end

  def clist
    if coin_types.present?
      COINS_LIST.select { |c| types_of_coins.include?(c) }.sort
    else
      COINS_LIST
    end
  end

  def types_of_coins
    coin_types.split(',').map(&:to_i)
  end

  def coin_types_in_coins_list
    unless types_of_coins.all? { |c| COINS_LIST.include?(c) }
      errors.add(:coin_types, 'incorrect!')
    end
  end
end
