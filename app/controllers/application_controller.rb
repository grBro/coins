class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  rescue_from CoinsExchange::CoinException, :with => :coin_not_found

  private

  def coin_not_found(error)
    render json: {:error => error.message,
                  box_remainder: session[:box_remainder]}
  end
end
