class CoinsExchangesController < ApplicationController
  def exchange_machine
    @coins = CoinsExchange.new(coins_exchange_params)
    session[:box_remainder] = nil if params[:delete_session] == '1'
    @coins.box_remainder_session_value = session[:box_remainder]
    if @coins.save
      result = @coins.exchange
      session[:box_remainder] = result[:box_remainder]
      render json: {change: result[:change],
                    box_remainder: session[:box_remainder]}
    else
      render json: {error: @coins.errors.full_messages }
    end
  end

  private

  def coins_exchange_params
    params.fetch(:coins_exchange)
          .permit(:amount, :coin_types)
  end
end
