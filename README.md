# Coins Exchange Machine

## Usage
Exchange without conditions:

    http://localhost:3000/?coins_exchange[amount]=225

Exchange with conditions:

    http://localhost:3000/?coins_exchange[amount]=225&coins_exchange[coin_types]=25,50

Delete session:

     http://localhost:3000/?coins_exchange[amount]=225&delete_session=1

