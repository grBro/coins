class CreateCoinsExchanges < ActiveRecord::Migration
  def change
    create_table :coins_exchanges do |t|
      t.integer :amount
      t.string :coin_types
    end
  end
end
